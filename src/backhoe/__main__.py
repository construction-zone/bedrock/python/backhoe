import argparse
import logging

from backhoe.module.cookiecutter.sync import CookiecutterSync
from backhoe.module.makefile.help import MakefileHelp

parser = argparse.ArgumentParser()
parser.add_argument("--make_list", nargs="*", action="append")
parser.add_argument("--cookiecutter_rsync", action="store_true")
parser.add_argument("--cookiecutter_template_path", type=str)
parser.add_argument("--cookiecutter_packages_path", type=str)
parser.add_argument("--artifact_path", type=str)
args = parser.parse_args()

if args.make_list is not None:
    """Creates makefile help output."""
    MakefileHelp(args)
elif args.cookiecutter_rsync:
    """Syncs pkgs with cookiecutter template."""
    if not args.cookiecutter_template_path:
        raise Exception("--cookiecutter_template_path required")
    if not args.cookiecutter_packages_path:
        raise Exception("--cookiecutter_packages_path required")
    if not args.artifact_path:
        raise Exception("--artifact_path required")
    with CookiecutterSync(
        template=args.cookiecutter_template_path,
        package_folder=args.cookiecutter_packages_path,
        artifact_path=args.artifact_path,
    ):
        logging.info("sync with template completed")
