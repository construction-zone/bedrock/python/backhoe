import glob
import logging
import pathlib
from difflib import ndiff
from fnmatch import fnmatch
from os import devnull, makedirs, walk
from os.path import abspath, dirname, isdir, isfile, join, relpath
from shutil import copyfile, rmtree
from sys import stdout
from tempfile import mkdtemp

from utility_belt import SubprocessExecutor


class CookiecutterSync:
    def __init__(self, template: str, package_folder: str, artifact_path: str):
        self.artifact_path = artifact_path
        root_path = abspath(join(self.artifact_path, ".."))
        self.template = join(root_path, template.lstrip("./"))
        self.package_folder = join(root_path, package_folder.lstrip("./"))

        # used to decided if the package folder is managed by cookiecutter
        self.cookiecutter_config = "cookiecutter.yaml"
        self.package_list = self._get_package_list()

        global_venv = join(artifact_path, "venv", "bin", "activate")
        self.venv = [f". {str(global_venv)}; "]

        self.executor = SubprocessExecutor(
            shell=True,
            stdout=stdout,
            allow_injection=self.venv,
        )

        self.ignore = [
            "*/src*",
            "*quickstart.py",
            "*test_examples.py",
            "*requirements*",
            "*CHANGELOG.md",
            "*MANIFEST.in",
            "*README.md",
        ]

    def __enter__(self):
        """Setup the environment."""
        self.tmp_dir = mkdtemp()
        logging.info(f"created a working folder: {self.tmp_dir}")

        self._setup_venv()

        for pkg_path in self.package_list:
            pkg = pathlib.PurePath(pkg_path).name
            self._run_cookiecutter(pkg, pkg_path)

            tmp_path = join(self.tmp_dir, pkg)
            if not isdir(tmp_path):
                logging.warning(
                    f"The {str(pkg)} folder is missing,"
                    f"check {self.cookiecutter_config} for a name mismatch"
                )
                continue

            for walker in walk(tmp_path):
                dirpath, _, filenames = walker
                for file in filenames:
                    full_path = join(dirpath, file)
                    if self._skip(full_path):
                        continue
                    rel_path = relpath(full_path, self.tmp_dir)
                    self._run_diff(rel_path, full_path)

    def __exit__(self, *args):
        """Clean up the tmp directory."""
        logging.info(f"removing the working folder: {self.tmp_dir}")
        rmtree(self.tmp_dir)

    def _setup_venv(self):
        """Makes sure cookiecutter is installed."""
        logging.info("installing cookiecutter into the venv")
        cookiecutter = self.venv + ["python3", "-m", "pip", "install", "cookiecutter"]

        with open(devnull, "w") as pipe:
            result = self.executor.execute(cookiecutter, stdout_pipe=pipe)

        if result.get("returncode"):
            raise Exception(
                f"command failed with returncode ({result.get('returncode')})"
            )

    def _get_package_list(self):
        """
        Grabs the paths for each package and filters out those not managed by
        cookiecutter.
        """
        package_list = []
        packages = glob.glob(f"{self.package_folder}/*")
        for pkg in packages:
            if isfile(join(pkg, self.cookiecutter_config)):
                package_list.append(pkg)
        return package_list

    def _run_cookiecutter(self, pkg: str, pkg_path: str):
        """
        Grabs the paths for each package and filters out those not managed by
        cookiecutter.
        """
        cookiecutter_config = join(pkg_path, self.cookiecutter_config)
        with open(cookiecutter_config):
            logging.info(f"starting sync up project {pkg} with template")
            cutter_cmd = self.venv + [
                "python3",
                "-m",
                "cookiecutter",
                "--no-input",
                "--overwrite-if-exists",
                "--output-dir",
                self.tmp_dir,
                "--config-file",
                cookiecutter_config,
                self.template,
            ]
            self.executor.execute(cutter_cmd)

    def _startswith(self, string, match=["+ ", "- ", "? "]):
        """Used to filter diff output based on the first char."""
        for c in match:
            if string.startswith(c):
                return True
        return False

    def _safe_copy(self, src, dest):
        """creates a folder then copies files."""
        makedirs(dirname(dest), exist_ok=True)
        copyfile(src, dest)

    def _run_diff(self, rel_path, template_path):
        """run and resolve diffs"""
        pgk_path = join(self.package_folder, rel_path)
        try:
            with open(template_path) as template, open(pgk_path) as working:
                if ".DS_Store" in template_path:
                    return

                diff = ndiff(working.readlines(), template.readlines())
                delta = "".join(line for line in diff if self._startswith(line))
                if not len(delta):
                    return
                print(
                    "-" * 20 + "\n",
                    f"{rel_path}\n",
                    f"{delta}\n",
                    "-" * 20 + "\n",
                )

        except Exception:
            self._safe_copy(template_path, pgk_path)
            return

        print("Apply this change? (y/N)")
        if input().lower().startswith("y"):
            self._safe_copy(template_path, pgk_path)

    def _skip(self, dirpath):
        match = False
        for skip in self.ignore:
            match = fnmatch(dirpath, skip)
            if match:
                break
        return match
