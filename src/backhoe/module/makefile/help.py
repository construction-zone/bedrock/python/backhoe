import re


class MakefileHelp:
    def __init__(self, args):
        if not args.make_list:
            raise Exception("Makefile list is missing missing")

        available = {}
        for makefile in args.make_list[0]:
            available.update(self._parse_makefile(makefile))

        filter_help = {}
        basic_help = {}
        for item in available:
            if "help" in item.split("/")[0].lower():
                basic_help[item] = available[item]
            else:
                filter_help[item] = available[item]

        self._pretty_print(basic_help)
        self._pretty_print(filter_help)

    def _parse_makefile(self, makefile):
        available = {}
        with open(makefile, "r") as opened_file:
            previous = ""
            for line in opened_file:
                if previous.startswith("##"):
                    cmd = self._parse_command(line)
                    if cmd:
                        available[cmd[0]] = self._parse_description(previous)
                previous = line
        return available

    def _parse_command(self, text):
        return re.findall(r"(^[a-zA-Z0-9_\-\/]+):", text)

    def _parse_description(self, text):
        text = text.strip("##")
        return text.strip()

    def _pretty_print(self, items):
        for cmd in sorted(items):
            desc = items[cmd]
            while len(cmd) < 30:
                cmd += " "
            while len(desc) < 60:
                desc += " "
            print(f"  \x1b[32;01m{cmd[0:35]}\x1b[0m {desc}")  # noqa: T001
