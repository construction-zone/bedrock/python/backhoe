"""A description of the package."""

import importlib.metadata

from utility_belt import logger_init

__version__ = importlib.metadata.version("backhoe")

logger_init()
