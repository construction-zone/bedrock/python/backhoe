backhoe
#######

A description of the package.

.. toctree::
   :maxdepth: 2
   :numbered:
   :hidden:

   user/index
   api/index
   dev/index


Quick Start
===========

backhoe is available on PyPI and can be installed with `pip <https://pip.pypa.io>`_.

.. code-block:: console

    $ pip install backhoe

After installing backhoe you can use it like any other Python module.

Here is a simple example:

.. code-block:: python

    import backhoe
    # Fill this section in with the common use-case.
