User Guide
##########

This section of the documentation provides user focused information such as
installing and quickly using this package.

.. _install-guide-label:

Install Guide
=============

.. note::

    It is best practice to install Python projects in a virtual environment,
    which can be created and activated as follows using Python 3.6+.

    .. code-block:: console

        $ make python/venv
        $ source venv/bin/activate
        (myvenv) $

The simplest way to install backhoe is using Pip.

.. code-block:: console

    $ pip install backhoe

This will install ``backhoe`` and all of its dependencies.


.. _api-reference-label:

API Reference
=============

The `API Reference <https://construction-zone.gitlab.io/bedrock/python/backhoe/api/index.html>`_ provides API-level documentation.


.. include:: ../../../CHANGELOG.rst
