backhoe.module package
======================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   backhoe.module.cookiecutter
   backhoe.module.makefile

Module contents
---------------

.. automodule:: backhoe.module
   :members:
   :undoc-members:
   :show-inheritance:
