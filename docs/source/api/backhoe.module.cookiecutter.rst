backhoe.module.cookiecutter package
===================================

Submodules
----------

backhoe.module.cookiecutter.sync module
---------------------------------------

.. automodule:: backhoe.module.cookiecutter.sync
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: backhoe.module.cookiecutter
   :members:
   :undoc-members:
   :show-inheritance:
