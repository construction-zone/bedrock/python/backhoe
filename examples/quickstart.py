"""
This example script imports the backhoe package and
prints out the version.
"""

import backhoe


def main():
    print(f"backhoe version: {backhoe.__version__}")


if __name__ == "__main__":
    main()
