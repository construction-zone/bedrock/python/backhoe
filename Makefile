# include Makefile artifacts from foundation
ARTIFACTS ?= ../../../artifacts
MAKE_MODULES = ${ARTIFACTS}/makefile/modules
MAKE_INCLUDE = ${MAKE_MODULES}/Makefile.*
-include ${MAKE_INCLUDE}

# setup the scope of the make help
HELP_FILTER = docker/|help|python/

# python environment variable defaults
PYTHON ?= python3.9
PYTHON_PACKAGE = backhoe

# docker environment variable defaults
DOCKER_IMAGE ?= ${PYTHON_PACKAGE}
DOCKER_TAG ?= dev
DOCKER_TARGET ?= final
DOCKER_WORKDIR ?= /app
DOCKER_ENTRYPOINT ?= ${DOCKER_WORKDIR}/venv/lib/${PYTHON}/site-packages/${PYTHON_PACKAGE}

# A failback if the foundation artifacts are not in scope
help::
	@if !(test -d ${ARTIFACTS};) \
	then \
		echo "\n" \
		"This project has a missing dependency on the foundation project\n" \
		"which can be found https://gitlab.com/construction-zone/bedrock/foundation \n" \
		"the expected file stucture should be: \n" \
		"\n" \
		"foundation \n" \
		"├───artifacts \n" \
		"├───packages \n" \
		"│   └───python \n" \
		"│       ├───backhoe \n" \
		"│       ├───utility_belt \n" \
		"|       └───>> ${PYTHON_PACKAGE} << \n" \
		; \
	fi
